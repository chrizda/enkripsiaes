/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aes;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author christian
 */
public class AES {

    	private static final char rijndael[] = { 0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe,
			0xd7, 0xab, 0x76, 0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72,
			0xc0, 0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15, 0x04,
			0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75, 0x09, 0x83, 0x2c,
			0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84, 0x53, 0xd1, 0x00, 0xed, 0x20,
			0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf, 0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33,
			0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8, 0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc,
			0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2, 0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e,
			0x3d, 0x64, 0x5d, 0x19, 0x73, 0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde,
			0x5e, 0x0b, 0xdb, 0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4,
			0x79, 0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08, 0xba,
			0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a, 0x70, 0x3e, 0xb5,
			0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e, 0xe1, 0xf8, 0x98, 0x11, 0x69,
			0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf, 0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42,
			0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16 };
        
       private static final char rijndaelinversed[] = { 0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81,
			0xf3, 0xd7, 0xfb, 0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9,
			0xcb, 0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e, 0x08,
			0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25, 0x72, 0xf8, 0xf6,
			0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92, 0x6c, 0x70, 0x48, 0x50, 0xfd,
			0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84, 0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3,
			0x0a, 0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3, 0x45, 0x06, 0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02, 0xc1,
			0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b, 0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf,
			0xce, 0xf0, 0xb4, 0xe6, 0x73, 0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c,
			0x75, 0xdf, 0x6e, 0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe,
			0x1b, 0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4, 0x1f,
			0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f, 0x60, 0x51, 0x7f,
			0xa9, 0x19, 0xb5, 0x4a, 0x0d, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef, 0xa0, 0xe0, 0x3b, 0x4d, 0xae,
			0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61, 0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6,
			0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d };
    
    
    
    public static String[] kestate(String a){
        byte[] b=a.getBytes();        
        String[] c = new String[b.length];
        for (int i = 0; i < b.length; i++) {
            c[i] = String.format("%02X", b[i]);            
        }        
        return c;
    }
    
    public static String stringkehex(String string) {
	return String.format("%x", new BigInteger(1, string.getBytes()));
    }
    
    public static byte[] hexstringkebyte(String hexString) {
	int panjang = hexString.length();
	byte[] data = new byte[panjang / 2]; //karena awalnya hex itu 00 -> 00 ini bakalan jadi 1 index, jadi panjang / 2
            for (int i = 0; i < panjang; i += 2) { //i+=2 kelipatan 2 karena hex itu 00 dan bakalan jadi 1
		data[i / 2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4) + Character.digit(hexString.charAt(i + 1), 16));
            } 
            //character.digit(hexString.charAt(i),16) 16(radix) ini karena mau jadi hex (hex kan 0123456789abcdef, jadi ada 16)
            //syntax character.digit ini buat ngrubah string jadi hex, di depan itu ada (byte) jadi bakalan jadi byte
            //<<4 ini buat left bitshift
        return data;
    }
    
    	public static String bytekehexstring(byte[] array) {
		String hexString = new String();
		for (byte hex : array){
			hexString += Integer.toString(hex & 0xFF, 16);
                }
		return hexString;
	}
           
    
    public static byte[][] arraykeMatrix(byte[] array) {
		byte[][] matrix = new byte[4][4];
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				matrix[j][i] = array[i * 4 + j];
		return matrix;
    }
    
    public static byte[][] kuncimatrix(byte[] array) {
		byte[][] matrix = new byte[4][4];
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				matrix[i][j] = array[i * 4 + j];
		return matrix;
    }
    
    public static byte[] matrixkearray(byte[][] matrix){
        byte[] array = new byte[16];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                array[i*4+j] = matrix[j][i];
            }
        }
        return array;
    }
        
    public static byte[][] SubBytes(byte[][] matrix, boolean inverted) {		
        char[] sbox = null;
		if(inverted == true){
                    sbox = rijndaelinversed;
                }
                else if(inverted == false){
                    sbox = rijndael;
                }
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				matrix[i][j] = (byte)sbox[matrix[i][j] & 0xff];
		
		return matrix;
	} 
    
    public static byte[] subbytebuatkunci(byte[] key){
        char[] sbox = rijndael;
        for (int i = 0; i < 4; i++) {
            key[i] = (byte)sbox[key[i] & 0xff];
        }
        return key;
    }
        
    public static byte[][] shiftrow(byte[][] matrix, boolean inverted){
        byte[] temp = new byte[4];
        int k=1;
            for (int i = 1; i < 4; i++) {                                      
                    if(inverted){
                        for (int j = 0; j < 4; j++)
                        temp[j]=matrix[i][(j+k)%4];
                    }
                    else{     
                        for (int j = 0; j < 4; j++)                                                        
                        temp[(j+k)%4]=matrix[i][j];                                                    
                    }            
                for (int j = 0; j < 4; j++)
                    matrix[i][j]=temp[j];                   
                k++;
        }
        return matrix;
    }
    
    	public static byte GFMult(byte a, byte b) {
		byte r = 0, t;
		while (a != 0) {
			if ((a & 1) != 0)
				r = (byte) (r ^ b);
			t = (byte) (b & 0x80);
			b = (byte) (b << 1);
			if (t != 0)
				b = (byte) (b ^ 0x1b);
			a = (byte) ((a & 0xff) >> 1);
		}
		return r;
	}
    
    public static byte[][] mixColumn(byte[][] matrix, boolean inverted){
        int[] temp = new int[4];
        byte a,b,c,d=0;
        if(inverted==false){
            a = (byte) 0x02;
            b = (byte) 0x03;
            c = (byte) 0x01;
            d = (byte) 0x01;
        }
        else{
            a = (byte) 0x0E;
            b = (byte) 0x0B;
            c = (byte) 0x0D;
            d = (byte) 0x09;            
        }
        for (int i = 0; i < 4; i++) {
            		temp[0] = GFMult(a, matrix[0][i]) ^ GFMult(b, matrix[1][i]) ^ GFMult(c, matrix[2][i]) ^ GFMult(d, matrix[3][i]);
			temp[1] = GFMult(d, matrix[0][i]) ^ GFMult(a, matrix[1][i]) ^ GFMult(b, matrix[2][i]) ^ GFMult(c, matrix[3][i]);
			temp[2] = GFMult(c, matrix[0][i]) ^ GFMult(d, matrix[1][i]) ^ GFMult(a, matrix[2][i]) ^ GFMult(b, matrix[3][i]);
			temp[3] = GFMult(b, matrix[0][i]) ^ GFMult(c, matrix[1][i]) ^ GFMult(d, matrix[2][i]) ^ GFMult(a, matrix[3][i]);
            for (int j = 0; j < 4; j++) {
                matrix[j][i] = (byte)temp[j];                
            }            
        }    
        System.out.println(matrix[0][0]);
        
        return matrix;
    }       
    
    public static byte[][] buatkeynya(String key){        
        byte[] key1 = hexstringkebyte(stringkehex(key));        
        byte[][] keyhex = kuncimatrix(key1);
        return keyhex;        
    }
    
    public static byte[] leftshift(byte[] key){
        byte [] temp = new byte[4];
        for (int i = 0; i < 4; i++) {           
            temp[i] = key[i];                        
        }        
        for (int i = 0; i < 4; i++) {
            key[i] = temp[(i+1)%4];
        }        
        return key;
    }
    
    public static byte[][] addroundkey (int ronde, byte[][] matrix, byte[][] key){        
        byte[][] hasil = new byte[4][4];
        char[] rcon = {0x00,0x0,0x0,0x0};
        switch (ronde) {
            case 1:
                rcon[0] = 0x01;
                break;
            case 2:
                rcon[0] = 0x02;
                break;
            case 3:
                rcon[0] = 0x04;
                break;
            case 4:
                rcon[0] = 0x08;
                break;
            case 5:
                rcon[0] = 0x10;
                break;
            case 6:
                rcon[0] = 0x20;
                break;
            case 7:
                rcon[0] = 0x40;
                break;
            case 8:
                rcon[0] = 0x80;
                break;
            case 9:
                rcon[0] = 0x1b;
                break;
            case 10:
                rcon[0] = 0x36;
                break;
        }        
        String[] rconhex = new String[4];
        for (int i = 0; i < 4; i++) {
            rconhex[i] = Integer.toHexString(rcon[i]);
        }                        
        leftshift(key[3]);
        subbytebuatkunci(key[3]);        
                
        for (int i = 0; i < 4; i++) {            
            key[3][i] = (byte) (Integer.parseInt(rconhex[i]) ^ key[3][i]);
        }                
        
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                hasil[i][j] = (byte) (matrix[i][j] ^ key[i][j]);
            }
        }        
        
        return hasil;
    }
    
    
    public static void main(String[] args) {             
        String text = "aesusesmatrixzzz";
        String key = "inikuncirahasiab";
        System.out.println(stringkehex(text));
        byte[] byteini = hexstringkebyte(stringkehex(text));
        byte[][] matrixx = arraykeMatrix(byteini);
        byte[][] sudahtahap1 = SubBytes(matrixx, false);
        System.out.println(Arrays.deepToString(sudahtahap1));
        System.out.println("=====================");
        byte[][] sudahtahap2 = shiftrow(sudahtahap1, false);
        System.out.println(Arrays.deepToString(sudahtahap2));
        byte[][] sudahtahap3 = mixColumn(sudahtahap2, false);
        System.out.println("tahap 3");
        System.out.println(Arrays.deepToString(sudahtahap3)); 
        byte[][] keyasli = buatkeynya(key);
        System.out.println(Arrays.deepToString(keyasli));                
        byte[][] sudahtahap4 = addroundkey(1, sudahtahap3, keyasli);
        System.out.println(Arrays.deepToString(sudahtahap4));
        
        
        
                                
        
//        ------------DECRYPT----------------        
        System.out.println("");
        System.out.println("Decrypt");
        System.out.println("");
        byte[][] dekrip3 = mixColumn(sudahtahap3, true);
        System.out.println("decrypt 3");
        System.out.println(Arrays.deepToString(dekrip3));
        byte[][] dekrip2 = shiftrow(dekrip3, true);
        System.out.println(Arrays.deepToString(dekrip2));
        byte[][] dekrip1 = SubBytes(dekrip2, true);        
        System.out.println("===========");
        System.out.println(Arrays.deepToString(dekrip1));        
        byte[] arraydekrip = matrixkearray(dekrip1);
        String dekripan = new String(arraydekrip,StandardCharsets.UTF_8);
        System.out.println("string dekrip");
        System.out.println(dekripan);
        
        
        
//            }
//        }
    }
    
}
